// =============================================================================
// Copyright 2011-2022 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef POPULATE_ITEMS_HPP
#define POPULATE_ITEMS_HPP

namespace populate_items
{
void make_items_on_floor();

}  // namespace populate_items

#endif  // POPULATE_ITEMS_HPP

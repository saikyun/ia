// =============================================================================
// Copyright 2011-2022 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef GAME_OVER_HPP
#define GAME_OVER_HPP

#include "global.hpp"

void on_game_over(IsWin is_win);

#endif  // GAME_OVER_HPP

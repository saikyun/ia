// =============================================================================
// Copyright 2011-2022 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "gfx.hpp"

#include <unordered_map>

#include "pos.hpp"

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------
using StrToTileIdMap = std::unordered_map<std::string, gfx::TileId>;

static const StrToTileIdMap s_str_to_tile_id_map = {
        {"aim_marker_head", gfx::TileId::aim_marker_head},
        {"aim_marker_line", gfx::TileId::aim_marker_line},
        {"alchemist_bench_empty", gfx::TileId::alchemist_bench_empty},
        {"alchemist_bench_full", gfx::TileId::alchemist_bench_full},
        {"altar", gfx::TileId::altar},
        {"ammo", gfx::TileId::ammo},
        {"amoeba", gfx::TileId::amoeba},
        {"amulet", gfx::TileId::amulet},
        {"ape", gfx::TileId::ape},
        {"armor", gfx::TileId::armor},
        {"astral_opium", gfx::TileId::astral_opium},
        {"axe", gfx::TileId::axe},
        {"barrel", gfx::TileId::barrel},
        {"bat", gfx::TileId::bat},
        {"blast1", gfx::TileId::blast1},
        {"blast2", gfx::TileId::blast2},
        {"bog_tcher", gfx::TileId::bog_tcher},
        {"bone_charm", gfx::TileId::bone_charm},
        {"bookshelf_empty", gfx::TileId::bookshelf_empty},
        {"bookshelf_full", gfx::TileId::bookshelf_full},
        {"brazier", gfx::TileId::brazier},
        {"bush", gfx::TileId::bush},
        {"byakhee", gfx::TileId::byakhee},
        {"cabinet_closed", gfx::TileId::cabinet_closed},
        {"cabinet_open", gfx::TileId::cabinet_open},
        {"chains", gfx::TileId::chains},
        {"chest_closed", gfx::TileId::chest_closed},
        {"chest_open", gfx::TileId::chest_open},
        {"chthonian", gfx::TileId::chthonian},
        {"church_bench", gfx::TileId::church_bench},
        {"clockwork", gfx::TileId::clockwork},
        {"club", gfx::TileId::club},
        {"cocoon_closed", gfx::TileId::cocoon_closed},
        {"cocoon_open", gfx::TileId::cocoon_open},
        {"corpse", gfx::TileId::corpse},
        {"corpse2", gfx::TileId::corpse2},
        {"crawling_hand", gfx::TileId::crawling_hand},
        {"crawling_intestines", gfx::TileId::crawling_intestines},
        {"croc_head_mummy", gfx::TileId::croc_head_mummy},
        {"crowbar", gfx::TileId::crowbar},
        {"crystal", gfx::TileId::crystal},
        {"cultist_dagger", gfx::TileId::cultist_dagger},
        {"cultist_pistol", gfx::TileId::cultist_pistol},
        {"cultist_pump_shotgun", gfx::TileId::cultist_pump_shotgun},
        {"cultist_rifle", gfx::TileId::cultist_rifle},
        {"cultist_sawed_off_shotgun", gfx::TileId::cultist_sawed_off_shotgun},
        {"cultist_spiked_mace", gfx::TileId::cultist_spiked_mace},
        {"cultist_tommygun", gfx::TileId::cultist_tommygun},
        {"dagger", gfx::TileId::dagger},
        {"deep_one", gfx::TileId::deep_one},
        {"device1", gfx::TileId::device1},
        {"device2", gfx::TileId::device2},
        {"door_closed", gfx::TileId::door_closed},
        {"door_open", gfx::TileId::door_open},
        {"door_stuck", gfx::TileId::door_stuck},
        {"dynamite", gfx::TileId::dynamite},
        {"dynamite_lit", gfx::TileId::dynamite_lit},
        {"elder_sign", gfx::TileId::elder_sign},
        {"excl_mark", gfx::TileId::excl_mark},
        {"failed_reanimation", gfx::TileId::failed_reanimation},
        {"fiend", gfx::TileId::fiend},
        {"flare", gfx::TileId::flare},
        {"flare_gun", gfx::TileId::flare_gun},
        {"flare_lit", gfx::TileId::flare_lit},
        {"floating_skull", gfx::TileId::floating_skull},
        {"floor", gfx::TileId::floor},
        {"fluct_material", gfx::TileId::fluctuating_material},
        {"fountain", gfx::TileId::fountain},
        {"fungi", gfx::TileId::fungi},
        {"gas_mask", gfx::TileId::gas_mask},
        {"gas_spore", gfx::TileId::gas_spore},
        {"gate_closed", gfx::TileId::gate_closed},
        {"gate_open", gfx::TileId::gate_open},
        {"gate_stuck", gfx::TileId::gate_stuck},
        {"ghost", gfx::TileId::ghost},
        {"ghoul", gfx::TileId::ghoul},
        {"giant_spider", gfx::TileId::giant_spider},
        {"glasuu", gfx::TileId::glasuu},
        {"gong", gfx::TileId::gong},
        {"gore1", gfx::TileId::gore1},
        {"gore2", gfx::TileId::gore2},
        {"gore3", gfx::TileId::gore3},
        {"gore4", gfx::TileId::gore4},
        {"gore5", gfx::TileId::gore5},
        {"gore6", gfx::TileId::gore6},
        {"gore7", gfx::TileId::gore7},
        {"gore8", gfx::TileId::gore8},
        {"grate", gfx::TileId::grate},
        {"grave_stone", gfx::TileId::grave_stone},
        {"hammer", gfx::TileId::hammer},
        {"hangbridge_hor", gfx::TileId::hangbridge_hor},
        {"hangbridge_ver", gfx::TileId::hangbridge_ver},
        {"holy_symbol", gfx::TileId::holy_symbol},
        {"horn", gfx::TileId::horn},
        {"hound", gfx::TileId::hound},
        {"hunting_horror", gfx::TileId::hunting_horror},
        {"incinerator", gfx::TileId::incinerator},
        {"iron_spike", gfx::TileId::iron_spike},
        {"khaga", gfx::TileId::khaga},
        {"lantern", gfx::TileId::lantern},
        {"leech", gfx::TileId::leech},
        {"leng_elder", gfx::TileId::leng_elder},
        {"lever_left", gfx::TileId::lever_left},
        {"lever_right", gfx::TileId::lever_right},
        {"lockpick", gfx::TileId::lockpick},
        {"locust", gfx::TileId::locust},
        {"machete", gfx::TileId::machete},
        {"mantis", gfx::TileId::mantis},
        {"mass_of_worms", gfx::TileId::mass_of_worms},
        {"medical_bag", gfx::TileId::medical_bag},
        {"mi_go", gfx::TileId::mi_go},
        {"mi_go_armor", gfx::TileId::mi_go_armor},
        {"mi_go_gun", gfx::TileId::mi_go_gun},
        {"molotov", gfx::TileId::molotov},
        {"monolith", gfx::TileId::monolith},
        {"mummy", gfx::TileId::mummy},
        {"ooze", gfx::TileId::ooze},
        {"orb", gfx::TileId::orb},
        {"phantasm", gfx::TileId::phantasm},
        {"pharaoh_staff", gfx::TileId::pharaoh_staff},
        {"pillar", gfx::TileId::pillar},
        {"pillar_broken", gfx::TileId::pillar_broken},
        {"pillar_carved", gfx::TileId::pillar_carved},
        {"pistol", gfx::TileId::pistol},
        {"pit", gfx::TileId::pit},
        {"pitchfork", gfx::TileId::pitchfork},
        {"player_firearm", gfx::TileId::player_firearm},
        {"player_melee", gfx::TileId::player_melee},
        {"player_unarmed", gfx::TileId::player_unarmed},
        {"polyp", gfx::TileId::polyp},
        {"potion", gfx::TileId::potion},
        {"projectile_std_back_slash", gfx::TileId::projectile_std_back_slash},
        {"projectile_std_dash", gfx::TileId::projectile_std_dash},
        {"projectile_std_front_slash", gfx::TileId::projectile_std_front_slash},
        {"projectile_std_vertical", gfx::TileId::projectile_std_vertical},
        {"pylon_angled", gfx::TileId::pylon_angled},
        {"pylon_arched", gfx::TileId::pylon_arched},
        {"pylon_coiled", gfx::TileId::pylon_coiled},
        {"pylon_serrated", gfx::TileId::pylon_serrated},
        {"pylon_star_crowned", gfx::TileId::pylon_star_crowned},
        {"pylon_two_pronged", gfx::TileId::pylon_two_pronged},
        {"rat", gfx::TileId::rat},
        {"rat_thing", gfx::TileId::rat_thing},
        {"raven", gfx::TileId::raven},
        {"revolver", gfx::TileId::revolver},
        {"rifle", gfx::TileId::rifle},
        {"ring", gfx::TileId::ring},
        {"rock", gfx::TileId::rock},
        {"rod", gfx::TileId::rod},
        {"rubble_high", gfx::TileId::rubble_high},
        {"rubble_low", gfx::TileId::rubble_low},
        {"sarcophagus", gfx::TileId::sarcophagus},
        {"scorched_ground", gfx::TileId::scorched_ground},
        {"scroll", gfx::TileId::scroll},
        {"shadow", gfx::TileId::shadow},
        {"shotgun", gfx::TileId::shotgun},
        {"sledge_hammer", gfx::TileId::sledge_hammer},
        {"smoke", gfx::TileId::smoke},
        {"snake", gfx::TileId::snake},
        {"spider", gfx::TileId::spider},
        {"spiked_mace", gfx::TileId::spiked_mace},
        {"square_checkered", gfx::TileId::square_checkered},
        {"square_checkered_sparse", gfx::TileId::square_checkered_sparse},
        {"stairs_down", gfx::TileId::stairs_down},
        {"stalagmite", gfx::TileId::stalagmite},
        {"tentacle_cluster", gfx::TileId::tentacle_cluster},
        {"the_high_priest", gfx::TileId::the_high_priest},
        {"tomb_closed", gfx::TileId::tomb_closed},
        {"tomb_open", gfx::TileId::tomb_open},
        {"tome", gfx::TileId::tome},
        {"tommy_gun", gfx::TileId::tommy_gun},
        {"torture_collar", gfx::TileId::torture_collar},
        {"trap_general", gfx::TileId::trap_general},
        {"trapez", gfx::TileId::trapez},
        {"tree", gfx::TileId::tree},
        {"tree_fungi", gfx::TileId::tree_fungi},
        {"troglodyte", gfx::TileId::troglodyte},
        {"vines", gfx::TileId::vines},
        {"void_traveler", gfx::TileId::void_traveler},
        {"vortex", gfx::TileId::vortex},
        {"wall_cave_front", gfx::TileId::wall_cave_front},
        {"wall_cave_top", gfx::TileId::wall_cave_top},
        {"wall_egypt_front", gfx::TileId::wall_egypt_front},
        {"wall_egypt_top", gfx::TileId::wall_egypt_top},
        {"wall_front", gfx::TileId::wall_front},
        {"wall_front_alt1", gfx::TileId::wall_front_alt1},
        {"wall_top", gfx::TileId::wall_top},
        {"water", gfx::TileId::water},
        {"web", gfx::TileId::web},
        {"witch_eye", gfx::TileId::witch_eye},
        {"witch_or_warlock", gfx::TileId::witch_or_warlock},
        {"wolf", gfx::TileId::wolf},
        {"worm", gfx::TileId::worm},
        {"wraith", gfx::TileId::wraith},
        {"zombie_armed", gfx::TileId::zombie_armed},
        {"zombie_bloated", gfx::TileId::zombie_bloated},
        {"zombie_dust", gfx::TileId::zombie_dust},
        {"zombie_unarmed", gfx::TileId::zombie_unarmed},
        {"", gfx::TileId::END}};

using TileIdToStrMap = std::unordered_map<gfx::TileId, std::string>;

static const TileIdToStrMap s_tile_id_to_str_map = {
        {gfx::TileId::aim_marker_head, "aim_marker_head"},
        {gfx::TileId::aim_marker_line, "aim_marker_line"},
        {gfx::TileId::alchemist_bench_empty, "alchemist_bench_empty"},
        {gfx::TileId::alchemist_bench_full, "alchemist_bench_full"},
        {gfx::TileId::altar, "altar"},
        {gfx::TileId::ammo, "ammo"},
        {gfx::TileId::amoeba, "amoeba"},
        {gfx::TileId::amulet, "amulet"},
        {gfx::TileId::ape, "ape"},
        {gfx::TileId::armor, "armor"},
        {gfx::TileId::astral_opium, "astral_opium"},
        {gfx::TileId::axe, "axe"},
        {gfx::TileId::barrel, "barrel"},
        {gfx::TileId::bat, "bat"},
        {gfx::TileId::blast1, "blast1"},
        {gfx::TileId::blast2, "blast2"},
        {gfx::TileId::bog_tcher, "bog_tcher"},
        {gfx::TileId::bone_charm, "bone_charm"},
        {gfx::TileId::bookshelf_empty, "bookshelf_empty"},
        {gfx::TileId::bookshelf_full, "bookshelf_full"},
        {gfx::TileId::brazier, "brazier"},
        {gfx::TileId::bush, "bush"},
        {gfx::TileId::byakhee, "byakhee"},
        {gfx::TileId::cabinet_closed, "cabinet_closed"},
        {gfx::TileId::cabinet_open, "cabinet_open"},
        {gfx::TileId::chains, "chains"},
        {gfx::TileId::chest_closed, "chest_closed"},
        {gfx::TileId::chest_open, "chest_open"},
        {gfx::TileId::chthonian, "chthonian"},
        {gfx::TileId::church_bench, "church_bench"},
        {gfx::TileId::clockwork, "clockwork"},
        {gfx::TileId::club, "club"},
        {gfx::TileId::cocoon_closed, "cocoon_closed"},
        {gfx::TileId::cocoon_open, "cocoon_open"},
        {gfx::TileId::corpse, "corpse"},
        {gfx::TileId::corpse2, "corpse2"},
        {gfx::TileId::crawling_hand, "crawling_hand"},
        {gfx::TileId::crawling_intestines, "crawling_intestines"},
        {gfx::TileId::croc_head_mummy, "croc_head_mummy"},
        {gfx::TileId::crowbar, "crowbar"},
        {gfx::TileId::crystal, "crystal"},
        {gfx::TileId::cultist_dagger, "cultist_dagger"},
        {gfx::TileId::cultist_pistol, "cultist_pistol"},
        {gfx::TileId::cultist_pump_shotgun, "cultist_pump_shotgun"},
        {gfx::TileId::cultist_rifle, "cultist_rifle"},
        {gfx::TileId::cultist_sawed_off_shotgun, "cultist_sawed_off_shotgun"},
        {gfx::TileId::cultist_spiked_mace, "cultist_spiked_mace"},
        {gfx::TileId::cultist_tommygun, "cultist_tommygun"},
        {gfx::TileId::dagger, "dagger"},
        {gfx::TileId::deep_one, "deep_one"},
        {gfx::TileId::device1, "device1"},
        {gfx::TileId::device2, "device2"},
        {gfx::TileId::door_closed, "door_closed"},
        {gfx::TileId::door_open, "door_open"},
        {gfx::TileId::door_stuck, "door_stuck"},
        {gfx::TileId::dynamite, "dynamite"},
        {gfx::TileId::dynamite_lit, "dynamite_lit"},
        {gfx::TileId::elder_sign, "elder_sign"},
        {gfx::TileId::excl_mark, "excl_mark"},
        {gfx::TileId::failed_reanimation, "failed_reanimation"},
        {gfx::TileId::fiend, "fiend"},
        {gfx::TileId::flare, "flare"},
        {gfx::TileId::flare_gun, "flare_gun"},
        {gfx::TileId::flare_lit, "flare_lit"},
        {gfx::TileId::floating_skull, "floating_skull"},
        {gfx::TileId::floor, "floor"},
        {gfx::TileId::fluctuating_material, "fluct_material"},
        {gfx::TileId::fountain, "fountain"},
        {gfx::TileId::fungi, "fungi"},
        {gfx::TileId::gas_mask, "gas_mask"},
        {gfx::TileId::gas_spore, "gas_spore"},
        {gfx::TileId::gate_closed, "gate_closed"},
        {gfx::TileId::gate_open, "gate_open"},
        {gfx::TileId::gate_stuck, "gate_stuck"},
        {gfx::TileId::ghost, "ghost"},
        {gfx::TileId::ghoul, "ghoul"},
        {gfx::TileId::giant_spider, "giant_spider"},
        {gfx::TileId::glasuu, "glasuu"},
        {gfx::TileId::gong, "gong"},
        {gfx::TileId::gore1, "gore1"},
        {gfx::TileId::gore2, "gore2"},
        {gfx::TileId::gore3, "gore3"},
        {gfx::TileId::gore4, "gore4"},
        {gfx::TileId::gore5, "gore5"},
        {gfx::TileId::gore6, "gore6"},
        {gfx::TileId::gore7, "gore7"},
        {gfx::TileId::gore8, "gore8"},
        {gfx::TileId::grate, "grate"},
        {gfx::TileId::grave_stone, "grave_stone"},
        {gfx::TileId::hammer, "hammer"},
        {gfx::TileId::hangbridge_hor, "hangbridge_hor"},
        {gfx::TileId::hangbridge_ver, "hangbridge_ver"},
        {gfx::TileId::holy_symbol, "holy_symbol"},
        {gfx::TileId::horn, "horn"},
        {gfx::TileId::hound, "hound"},
        {gfx::TileId::hunting_horror, "hunting_horror"},
        {gfx::TileId::incinerator, "incinerator"},
        {gfx::TileId::iron_spike, "iron_spike"},
        {gfx::TileId::khaga, "khaga"},
        {gfx::TileId::lantern, "lantern"},
        {gfx::TileId::leech, "leech"},
        {gfx::TileId::leng_elder, "leng_elder"},
        {gfx::TileId::lever_left, "lever_left"},
        {gfx::TileId::lever_right, "lever_right"},
        {gfx::TileId::lockpick, "lockpick"},
        {gfx::TileId::locust, "locust"},
        {gfx::TileId::machete, "machete"},
        {gfx::TileId::mantis, "mantis"},
        {gfx::TileId::mass_of_worms, "mass_of_worms"},
        {gfx::TileId::medical_bag, "medical_bag"},
        {gfx::TileId::mi_go, "mi_go"},
        {gfx::TileId::mi_go_armor, "mi_go_armor"},
        {gfx::TileId::mi_go_gun, "mi_go_gun"},
        {gfx::TileId::molotov, "molotov"},
        {gfx::TileId::monolith, "monolith"},
        {gfx::TileId::mummy, "mummy"},
        {gfx::TileId::ooze, "ooze"},
        {gfx::TileId::orb, "orb"},
        {gfx::TileId::phantasm, "phantasm"},
        {gfx::TileId::pharaoh_staff, "pharaoh_staff"},
        {gfx::TileId::pillar, "pillar"},
        {gfx::TileId::pillar_broken, "pillar_broken"},
        {gfx::TileId::pillar_carved, "pillar_carved"},
        {gfx::TileId::pistol, "pistol"},
        {gfx::TileId::pit, "pit"},
        {gfx::TileId::pitchfork, "pitchfork"},
        {gfx::TileId::player_firearm, "player_firearm"},
        {gfx::TileId::player_melee, "player_melee"},
        {gfx::TileId::player_unarmed, "player_unarmed"},
        {gfx::TileId::polyp, "polyp"},
        {gfx::TileId::potion, "potion"},
        {gfx::TileId::projectile_std_back_slash, "projectile_std_back_slash"},
        {gfx::TileId::projectile_std_dash, "projectile_std_dash"},
        {gfx::TileId::projectile_std_front_slash, "projectile_std_front_slash"},
        {gfx::TileId::projectile_std_vertical, "projectile_std_vertical"},
        {gfx::TileId::pylon_angled, "pylon_angled"},
        {gfx::TileId::pylon_arched, "pylon_arched"},
        {gfx::TileId::pylon_coiled, "pylon_coiled"},
        {gfx::TileId::pylon_serrated, "pylon_serrated"},
        {gfx::TileId::pylon_star_crowned, "pylon_star_crowned"},
        {gfx::TileId::pylon_two_pronged, "pylon_two_pronged"},
        {gfx::TileId::rat, "rat"},
        {gfx::TileId::rat_thing, "rat_thing"},
        {gfx::TileId::raven, "raven"},
        {gfx::TileId::revolver, "revolver"},
        {gfx::TileId::rifle, "rifle"},
        {gfx::TileId::ring, "ring"},
        {gfx::TileId::rock, "rock"},
        {gfx::TileId::rod, "rod"},
        {gfx::TileId::rubble_high, "rubble_high"},
        {gfx::TileId::rubble_low, "rubble_low"},
        {gfx::TileId::sarcophagus, "sarcophagus"},
        {gfx::TileId::scorched_ground, "scorched_ground"},
        {gfx::TileId::scroll, "scroll"},
        {gfx::TileId::shadow, "shadow"},
        {gfx::TileId::shotgun, "shotgun"},
        {gfx::TileId::sledge_hammer, "sledge_hammer"},
        {gfx::TileId::smoke, "smoke"},
        {gfx::TileId::snake, "snake"},
        {gfx::TileId::spider, "spider"},
        {gfx::TileId::spiked_mace, "spiked_mace"},
        {gfx::TileId::square_checkered, "square_checkered"},
        {gfx::TileId::square_checkered_sparse, "square_checkered_sparse"},
        {gfx::TileId::stairs_down, "stairs_down"},
        {gfx::TileId::stalagmite, "stalagmite"},
        {gfx::TileId::tentacle_cluster, "tentacle_cluster"},
        {gfx::TileId::the_high_priest, "the_high_priest"},
        {gfx::TileId::tomb_closed, "tomb_closed"},
        {gfx::TileId::tomb_open, "tomb_open"},
        {gfx::TileId::tome, "tome"},
        {gfx::TileId::tommy_gun, "tommy_gun"},
        {gfx::TileId::torture_collar, "torture_collar"},
        {gfx::TileId::trap_general, "trap_general"},
        {gfx::TileId::trapez, "trapez"},
        {gfx::TileId::tree, "tree"},
        {gfx::TileId::tree_fungi, "tree_fungi"},
        {gfx::TileId::troglodyte, "troglodyte"},
        {gfx::TileId::vines, "vines"},
        {gfx::TileId::void_traveler, "void_traveler"},
        {gfx::TileId::vortex, "vortex"},
        {gfx::TileId::wall_cave_front, "wall_cave_front"},
        {gfx::TileId::wall_cave_top, "wall_cave_top"},
        {gfx::TileId::wall_egypt_front, "wall_egypt_front"},
        {gfx::TileId::wall_egypt_top, "wall_egypt_top"},
        {gfx::TileId::wall_front, "wall_front"},
        {gfx::TileId::wall_front_alt1, "wall_front_alt1"},
        {gfx::TileId::wall_top, "wall_top"},
        {gfx::TileId::water, "water"},
        {gfx::TileId::web, "web"},
        {gfx::TileId::witch_eye, "witch_eye"},
        {gfx::TileId::witch_or_warlock, "witch_or_warlock"},
        {gfx::TileId::wolf, "wolf"},
        {gfx::TileId::worm, "worm"},
        {gfx::TileId::wraith, "wraith"},
        {gfx::TileId::zombie_armed, "zombie_armed"},
        {gfx::TileId::zombie_bloated, "zombie_bloated"},
        {gfx::TileId::zombie_dust, "zombie_dust"},
        {gfx::TileId::zombie_unarmed, "zombie_unarmed"},
        {gfx::TileId::END, ""}};

// -----------------------------------------------------------------------------
// gfx
// -----------------------------------------------------------------------------
namespace gfx
{
P character_pos(const char character)
{
        return {character - ' ', 0};
}

TileId str_to_tile_id(const std::string& str)
{
        return s_str_to_tile_id_map.at(str);
}

std::string tile_id_to_str(TileId id)
{
        return s_tile_id_to_str_map.at(id);
}

}  // namespace gfx
